/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "MidiEngine.hpp"

#include <QApplication>
#include <QDebug>
#include <QStringList>

#include <jack/jack.h>
#include <jack/midiport.h>

#include <stdexcept>
#include <cassert>

#include "ControlEvent.hpp"
#include "MidiEnginePrivate.hpp"

namespace Ivory
{
    MidiEngine::MidiEngine() :
        _d( new MidiEnginePrivate )
    {
    }

    MidiEngine::~MidiEngine()
    {
        deactivate();
        deinitialize();
    }

    void MidiEngine::initialize()
    {
        jack_status_t jack_status;
        int rv;

        _d->client = jack_client_open(
            "Ivory",
            JackNullOption,
            &jack_status
            );
        if( ! _d->client )
            throw std::runtime_error("Could not set up JACK");

        _d->port = jack_port_register( _d->client,
                                      "midi_out",
                                      JACK_DEFAULT_MIDI_TYPE,
                                      JackPortIsOutput,
                                      0 );
        if( ! _d->port )
            throw std::runtime_error("Could not set up JACK port");

        rv = jack_set_process_callback( _d->client,
                                        MidiEngine::static_jack_callback,
                                        this );
        if( rv )
            throw std::runtime_error("Could not set JACK process callback");

        _d->queue.reset();
    }

    void MidiEngine::activate()
    {
        if(_d->client)
            jack_activate(_d->client);
    }

    void MidiEngine::deactivate()
    {
        if(_d->client)
            jack_deactivate(_d->client);
    }

    void MidiEngine::deinitialize()
    {
        if(_d->port) {
            assert(_d->client);
            jack_port_unregister(_d->client, _d->port);
            _d->port = 0;
        }
        if(_d->client) {
            jack_client_close(_d->client);
            _d->client = 0;
        }
    }

    typedef struct _queue_data_t {
        jack_time_t timestamp; // from jack_get_time()
        unsigned size; // Number of bytes of data following
        /* Data goes here */
    } queue_data_t;

    void MidiEngine::control_event(const ControlEvent& ev)
    {
        queue_data_t data;
        byte_t buf[64];

        data.timestamp = ev.timestamp();
        switch(ev.type()) {
        case ControlEvent::NoteEvent: {
            const NoteEvent& nev = static_cast<const NoteEvent&>(ev);
            data.size = 3;
            if(nev.note_event_type() == NoteEvent::NoteOn) {
                buf[0] = 0x90;
            } else {
                buf[0] = 0x80;
            }
            buf[1] = unsigned(nev.note()) & 0x7F;
            buf[2] = 0x7F & unsigned(255 * nev.velocity());
        }   break;
        case ControlEvent::CcEvent: {
            const CcEvent& cev = static_cast<const CcEvent&>(ev);
            data.size = 3;
            buf[0] = 0xB0;
            buf[1] = unsigned(cev.controller()) & 0x7F;
            buf[2] = unsigned(cev.value()) & 0x7F;
        }   break;
        case ControlEvent::PitchEvent: {
            const PitchEvent& pev = static_cast<const PitchEvent&>(ev);
            unsigned val = (1.0f + pev.value()) * float(0x2000) + 0.5f;
            if(pev.value() >= 1.0f)
                val = ~0;
            else if (pev.value() <= -1.0f)
                val = 0;
            data.size = 3;
            buf[0] = 0xE0;
            buf[1] = val & 0x7F;
            buf[2] = (val >> 7) & 0x7F;
            qDebug() << hex << val;
            qDebug() << hex << buf[0] << buf[1] << buf[2];
        }   break;
        default:
            assert(false);
        }
        unsigned need_space = sizeof(data) + data.size;
        QMutexLocker lck(&_d->queue_write_mutex);
        while( need_space > _d->queue.write_space() ) {
            usleep(500);
        }
        unsigned tmp;
        tmp = _d->queue.write(reinterpret_cast<byte_t*>(&data), sizeof(queue_data_t));
        assert(tmp == sizeof(queue_data_t));
        tmp = _d->queue.write(buf, data.size);
        assert(tmp == data.size);
    }

    int MidiEngine::jack_callback(jack_nframes_t nframes)
    {
        Tritium::RingBuffer<byte_t>::rw_vector read_vec;
        const unsigned SCRATCHSIZE = 64 + sizeof(queue_data_t);
        byte_t scratch[SCRATCHSIZE];
        queue_data_t *meta = reinterpret_cast<queue_data_t*>(scratch);
        byte_t *data = scratch + sizeof(queue_data_t);
        jack_midi_data_t *buf = (jack_midi_data_t*)jack_port_get_buffer(_d->port, nframes);

        assert(buf);
        jack_midi_clear_buffer(buf);

        _d->queue.get_read_vector(&read_vec);
        unsigned ctr, j, k;
        enum { META, DATA, FULL } state = META;
        ctr = 0;
        for( j=0 ; j<2 ; ++j ) {
            for( k=0 ; k < read_vec.len[j] ; ++k ) {
                if( state == META ) {
                    assert( ctr < sizeof(queue_data_t) );
                    scratch[ctr] = read_vec.buf[j][k];
                    ++ctr;
                    if( ctr == sizeof(queue_data_t) )
                        state = DATA;
                } else {
                    assert( ctr < SCRATCHSIZE );
                    assert(state == DATA);
                    scratch[ctr] = read_vec.buf[j][k];
                    ++ctr;
                    if( ctr == meta->size + sizeof(queue_data_t) )
                        state = FULL;
                }

                if( state == FULL ) {
                    assert( data[0] & 0x80 ); // Check status bit
                    jack_nframes_t time = _d->convert_timestamp_to_jack_time(meta->timestamp, nframes);
                    if( jack_midi_max_event_size(buf) > meta->size ) {
                        jack_midi_event_write( buf,
                                               time,
                                               (jack_midi_data_t*)data,
                                               meta->size );
                    }
                    state = META;
                    ctr = 0;
                }
            }
        }
        // There should be no stragglers.
        assert(ctr == 0);
        assert(state == META);
        _d->queue.increment_read_idx(read_vec.len[0] + read_vec.len[1]);

        return 0;
    }

    /* MUST BE CALLED FROM JACK process()
     *
     * jack_last_frame_time() returns the time at /start/ of this
     * process cycle.  Any events in the queue probably happened
     * /before/ that... but after the previous process cycle started.
     * Therefore, after jack converts usecs to frames, we add `nframes`
     * to it to map it into the current process cycle.
     *
     * If it lands outside... we clamp it to the current process cycle.
     */
    jack_nframes_t MidiEnginePrivate::convert_timestamp_to_jack_time(
        jack_time_t timestamp,
        jack_nframes_t nframes
        )
    {
        jack_nframes_t ev, cycle;
        ev = jack_time_to_frames(client, timestamp);
        cycle = jack_last_frame_time(client);
        int64_t diff;
        diff = ((int64_t)(ev)) - ((int64_t)(cycle));
        ev += nframes;
        if( ev < cycle ) {
            ev = 0;
        } else {
            ev -= cycle;
        }
        if( ev >= nframes ) {
            ev = nframes - 1;
        }
        assert( ev < nframes );
        return ev;
    }

    void MidiEngine::query_port_list(QStringList& dest)
    {
        const char** jlist;

        dest.clear();
        jlist = jack_get_ports( _d->client,
                                0,
                                JACK_DEFAULT_MIDI_TYPE,
                                JackPortIsInput );
        if(jlist) {
            const char **ptr;
            for( ptr = jlist ; *ptr ; ++ptr ) {
                dest << QString( *ptr );
            }
            free(jlist);
        }
    }

    void MidiEngine::connect_to_port(const QString& dest)
    {
        if( dest != "" ) {
            QString src( jack_port_name(_d->port) );
            if( jack_connect( _d->client,
                              src.toLatin1(),
                              dest.toLatin1() ) ) {
                qDebug() << "Could not connect ports: " << src << " --> " << dest;
            }            
        } else {
            jack_port_disconnect(_d->client, _d->port);
        }
    }

} // namespace Ivory
