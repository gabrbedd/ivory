/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "OctaveWidget.hpp"

#include <QDebug>
#include <QFontMetrics>
#include <QGraphicsSceneResizeEvent>
#include <QPainter>
#include <QPixmap>

namespace Ivory
{
    OctaveWidget::OctaveWidget(QGraphicsWidget *parent) :
        SloppyTouchWidget(parent),
        _octave(0),
        _font("Sans", 26),
        _image( new QPixmap(":/img/octave_widget.png") )
    {
        QSizePolicy pol(QSizePolicy::Preferred, QSizePolicy::Preferred );
        pol.setHeightForWidth(true);
        setSizePolicy(pol);
        setMinimumSize(30, 10);
    }

    OctaveWidget::~OctaveWidget()
    {
    }

    void OctaveWidget::paint(QPainter *p,
                             const QStyleOptionGraphicsItem *,
                             QWidget *)
    {
        p->drawPixmap( rect(), *_image, _image->rect() );

        QFontMetricsF fm(_font);
        QString txt = QString::number(_octave);
        QRectF text_box = QRectF( rect().width()/3.0,
                                  0,
                                  rect().width()/3.0,
                                  rect().height() );
        p->setBrush(QColor(Qt::black));
        p->setFont(_font);
        p->drawText(text_box,
                    txt,
                    Qt::AlignCenter | Qt::AlignVCenter);
    }

    QSizeF OctaveWidget::sizeHint(Qt::SizeHint which,
                                  const QSizeF& constraint) const
    {
        float h, w;
        if(constraint.height() != -1) {
            h = constraint.height();
        } else {
            h = 50.0;
        }
        if(constraint.width() != -1) {
            w = constraint.width();
        } else {
            w = 3.0 * h;
        }
        QSizeF rv(w,h);
        return rv;
    }

    bool OctaveWidget::touchEvent(Ivory::TouchEvent& ev)
    {
        TouchButtonLogic::state_t state;
        TouchButtonLogic::event_t event;
        QRectF down, up;

        down = rect();
        down.setWidth( down.width()/3.0 );
        up = down;
        up.translate( 2.0 * down.width(), 0 );

        state = _up_logic.touchEvent(ev, up, &event);
        if(event == TouchButtonLogic::CLICKED)
            emit octave_up();

        state = _dn_logic.touchEvent(ev, down, &event);
        if(event == TouchButtonLogic::CLICKED)
            emit octave_down();

        return true;
    }

    void OctaveWidget::set_octave(int o)
    {
        _octave = o;
        update();
    }

} // namespace Ivory
