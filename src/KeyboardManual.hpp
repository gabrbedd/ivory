/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_KEYBOARDMANUAL_HPP_
#define _IVORY_KEYBOARDMANUAL_HPP_

#include <QGraphicsWidget>
#include <memory>

#include "SloppyTouchWidget.hpp"

class QEvent;
class QPaintEvent;

namespace Ivory
{

    class ControlEventCallback;
    class KeyboardModel;
    class KeyboardView;
    class TouchEvent;

    class KeyboardManual : public SloppyTouchWidget
    {
        Q_OBJECT
    public:
        KeyboardManual(QGraphicsWidget* parent = 0,
                       int nkeys = 18,
                       int c = 2);
        ~KeyboardManual();

        bool touchEvent(Ivory::TouchEvent& ev);
        void set_callback(ControlEventCallback *cb);

        /**
         * /brief set range of valid octave for controller
         *
         * Octave 0 has C = Note 60 (Middle C).  Default is [-5,+5].
         */
        void set_octave_range(int min, int max);

    public slots:
        void set_octave(int o);
        void octave_up();
        void octave_down();

    signals:
        void octave_changed(int o);

    private:
        virtual bool event(QEvent* e);
        virtual void touchEvent(QTouchEvent *ev);
        virtual void paint(QPainter *painter,
                           const QStyleOptionGraphicsItem *option,
                           QWidget *widget);

        int key_for_coord(const QPointF& pos);

    private:
        std::auto_ptr<KeyboardModel> _model;
        std::auto_ptr<KeyboardView> _view;
        ControlEventCallback *_callback;
    };

} // namespace Ivory

#endif // _IVORY_KEYBOARDMANUAL_HPP_
