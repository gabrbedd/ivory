/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_APPLICATION_HPP_
#define _IVORY_APPLICATION_HPP_

#include <QGraphicsView>
#include <QMutex>

#include <memory>

#include "ControlEvent.hpp"

namespace Ivory
{
    class Scene;
    class MidiEngine;
    class TopBar;

    class Application : public QGraphicsView
    {
        Q_OBJECT
    public:
        Application(int argc, char* argv[]);
        ~Application();

    public:
        class Callback : public ControlEventCallback
        {
            MidiEngine *_midi;
        public:
            Callback(MidiEngine *target) : _midi(target) {}

            void set_target(MidiEngine* target) {
                _midi = target;
            }

            void operator()(const ControlEvent& ev);
        };

    public slots:
        void connect_to_port(const QString& dest);

    signals:
        void quit();

    protected:
        virtual void resizeEvent(QResizeEvent *ev);

    private:
        // Top-level widget
        Scene *_scene;

        // MIDI specifics
        std::auto_ptr<MidiEngine> _midi;
        QMutex _queue_mutex;
        Callback _functor;
    };

} // namespace Ivory

#endif // _IVORY_APPLICATION_HPP_
