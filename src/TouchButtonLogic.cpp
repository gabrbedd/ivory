/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "TouchButtonLogic.hpp"
#include "TouchEvent.hpp"

#include <QDebug>
#include <QRectF>

#include <cassert>

namespace Ivory
{
    TouchButtonLogic::TouchButtonLogic() :
        _state(TouchButtonLogic::RELEASED)
    {
    }

    TouchButtonLogic::~TouchButtonLogic()
    {
    }

    /*
     * State Chart

     STATE     INPUT             OUTPUT   NEXT STATE  OTHER
     --------------------------------------------------------
     RELEASED  Pressed/In        NONE     PRESSED     Latch ID
               Moved/In          NONE     RELEASED
               Stationary/In     NONE     RELEASED
               Released/In       NONE     RELEASED
               Pressed/Out       NONE     RELEASED
               Moved/Out         NONE     RELEASED
               Stationary/Out    NONE     RELEASED
               Released/Out      NONE     RELEASED

     PRESSED   Id=Pressed/In     NONE     PRESSED
               Id=Moved/In       NONE     PRESSED
               Id=Stationary/In  NONE     PRESSED
               Id=Released/In    CLICKED  RELEASED
               Id=Pressed/Out    NONE     RELEASED 
               Id=Moved/Out      NONE     PRESSED
               Id=Stationary/Out NONE     PRESSED
               Id=Released/Out   NONE     RELEASED
               Id is missing     NONE     RELEASED
     */
    TouchButtonLogic::state_t TouchButtonLogic::touchEvent(
        Ivory::TouchEvent& ev,
        QRectF area,
        event_t* out)
    {
        TouchEvent::Iterator it;
        bool id_missing = true;
        event_t output = NONE;
        bool is_inside = false;
        bool done = false;

        for(it = ev.begin() ; it != ev.end() ; ++it)
        {
            if( area.contains( it->pos() ) )
                is_inside = true;

            if(_state == PRESSED) {
                if(it->id() == _tp_id) {
                    id_missing = false;
                } else {
                    continue;
                }
                switch( it->state() ) {
                case Qt::TouchPointPressed:
                    if(!is_inside) {
                        _state = RELEASED;
                    }
                    break;
                case Qt::TouchPointMoved:
                case Qt::TouchPointStationary:
                    break;
                case Qt::TouchPointReleased:
                    _state = RELEASED;
                    if(is_inside)
                        output = CLICKED;
                    break;
                default:
                    assert(false);
                }
            } else if (_state == RELEASED) {
                id_missing = false;
                switch( it->state() ) {
                case Qt::TouchPointPressed:
                    if(is_inside) {
                        _state = PRESSED;
                        _tp_id = it->id();
                    }
                    break;
                case Qt::TouchPointMoved:
                case Qt::TouchPointStationary:
                case Qt::TouchPointReleased:
                    break;
                default:
                    assert(false);
                }
            } else {
                assert(false);
            }

            if(done) break;
        }

        if((_state == PRESSED) && id_missing) {
            _state = RELEASED;
            output = NONE;
        }

        if(out) *out = output;
        return _state;
    }

} // namespace Ivory
