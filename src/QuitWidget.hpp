/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_QUITWIDGET_HPP_
#define _IVORY_QUITWIDGET_HPP_

#include <QGraphicsWidget>
#include <QSizeF>
#include <QTouchEvent>
#include <memory>

#include "SloppyTouchWidget.hpp"
#include "TouchEvent.hpp"
#include "TouchButtonLogic.hpp"

class QPixmap;

namespace Ivory
{
    /**
     * /brief Container for the widgets at top of the controller
     */
    class QuitWidget : public SloppyTouchWidget
    {
        Q_OBJECT
    public:
        QuitWidget(QGraphicsWidget *parent);
        ~QuitWidget();

        virtual bool touchEvent(Ivory::TouchEvent& ev);

    signals:
        void quit();

    protected:
        virtual void paint(QPainter *painter,
                           const QStyleOptionGraphicsItem *option,
                           QWidget *widget);

        virtual QSizeF sizeHint(Qt::SizeHint which,
                                const QSizeF& constraint = QSizeF() ) const;
    private:
        TouchButtonLogic _logic;
        std::auto_ptr<QPixmap> _image;
    };

} // namespace Ivory

#endif // _IVORY_QUITWIDGET_HPP_
