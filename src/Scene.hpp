/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_SCENE_HPP_
#define _IVORY_SCENE_HPP_

#include <QGraphicsScene>
#include "TouchRouterLogic.hpp"
#include <memory>

class QComboBox;
class QGraphicsLinearLayout;
class QGraphicsProxyWidget;
class QGraphicsSceneResizeEvent;
class QGraphicsSimpleTextItem;
class QGraphicsWidget;
class QObject;
class QStringList;
class QStringListModel;
class QTouchEvent;

namespace Ivory
{

    class ControlEventCallback;
    class KeyboardManual;
    /*class TopBar;*/
    class Wheel;
    class TouchEvent;
    class OctaveWidget;
    class QuitWidget;

    class Scene : public QGraphicsScene
    {
        Q_OBJECT
    public:
        typedef QGraphicsScene Parent_t;

        Scene(QObject *parent = 0);
        ~Scene();

        void set_callback(ControlEventCallback *cb);
        void resize(int x, int y);
        void set_avail_ports(const QStringList& list);

    protected:
        virtual bool event(QEvent* ev);
        virtual void drawBackground(QPainter *p, const QRectF& r);

    public slots:
        void set_octave(int o);
        void octave_up();
        void octave_down();

    signals:
        void octave_changed(int o);
        void quit();
        void request_connect_to_port(const QString& p);

    private:
        TouchRouterLogic         _router;

        /* top bar */
        QGraphicsSimpleTextItem *_lbl_octave;
        OctaveWidget *_octave;
        QComboBox *_ports;
        QGraphicsProxyWidget *_ports_proxy;
        QStringListModel *_ports_list;
        QuitWidget *_quit;
        
        KeyboardManual      *_kb;
        Wheel               *_wh;

        ControlEventCallback  *_callback;
        std::auto_ptr<QPixmap> _background_image;
    };

} // namespace Ivory

#endif // _IVORY_SCENE_HPP_
