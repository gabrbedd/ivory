/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_MIDIENGINE_HPP_
#define _IVORY_MIDIENGINE_HPP_

#include <jack/types.h>

class QString;
class QStringList;

namespace Ivory
{

    class MidiEnginePrivate;
    class ControlEvent;

    class MidiEngine
    {
    public:
        typedef unsigned char byte_t;

        MidiEngine();
        virtual ~MidiEngine();

        void initialize();
        void activate();
        void deactivate();
        void deinitialize();

        void control_event(const ControlEvent& ev);

        void query_port_list( QStringList& dest );
        void connect_to_port(const QString& name);

    private:
        static int static_jack_callback(jack_nframes_t nframes, void* arg) {
            return static_cast<MidiEngine*>(arg)->jack_callback(nframes);
        }

        int jack_callback(jack_nframes_t nframes);

    private:
        MidiEnginePrivate * const _d;
    };

} // namespace Ivory

#endif // _IVORY_MIDIENGINE_HPP_
