/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_CONTROLEVENT_HPP_
#define _IVORY_CONTROLEVENT_HPP_

#include <stdint.h>

namespace Ivory
{

    class ControlEvent
    {
    public:
        typedef enum { NoteEvent,
                       CcEvent,
                       PitchEvent } type_t;

        typedef uint64_t timestamp_t;

    public:
        ControlEvent();
        virtual ~ControlEvent() {}

        virtual type_t type() const = 0;
        const timestamp_t& timestamp() const {
            return _timestamp;
        }
        void update_timestamp();

    private:
        timestamp_t _timestamp;
    };

    class ControlEventCallback
    {
    public:
        virtual ~ControlEventCallback() {}

        virtual void operator()(const ControlEvent& ev) = 0;
    };

    class NoteEvent : public ControlEvent
    {
    public:
        typedef enum { NoteOn,
                       NoteOff } note_event_t;

    public:
        NoteEvent();
        virtual ~NoteEvent();

        virtual type_t type() const { return ControlEvent::NoteEvent; }

        note_event_t note_event_type() const { return _type; }
        void set_note_event_type(note_event_t t) { _type = t; }
        int note() const { return _note; }
        void note(int n);
        float velocity() const { return _velocity; }
        void velocity(float v);

    private:
        note_event_t _type;
        int _note;
        float _velocity; // Range [0.0, 1.0]
    };

    class CcEvent : public ControlEvent
    {
    public:
        CcEvent();
        virtual ~CcEvent();

        virtual type_t type() const { return ControlEvent::CcEvent; }
        int controller() const { return _cc; }
        void controller(int cc);
        int value() const { return _val; }
        void value(int v);
    private:
        int _cc;
        int _val;
    };

    class PitchEvent : public ControlEvent
    {
    public:
        PitchEvent(float v = 0.0f);
        virtual ~PitchEvent();

        virtual type_t type() const { return ControlEvent::PitchEvent; }
        float value() const {// range: [-1.0, 1.0]
            return _val;
        }
        void value(float v);
    private:
        float _val;
    };

} // namespace Ivory

#endif // _IVORY_CONTROLEVENT_HPP_
