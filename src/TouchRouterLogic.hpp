/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_TOUCHROUTERLOGIC_HPP_
#define _IVORY_TOUCHROUTERLOGIC_HPP_

#include <QSet>
#include <QTouchEvent>

#include "TouchEvent.hpp"

namespace Ivory
{
    class SloppyTouchWidget;

    /**
     * /brief Class that routes QTouchEvents to sub-widgets.
     */
    class TouchRouterLogic
    {
    public:
        TouchRouterLogic();
        virtual ~TouchRouterLogic();

        virtual bool event(QEvent *ev);
        virtual bool touchEvent(Ivory::TouchEvent& ev);

        void add_sloppy_widget(SloppyTouchWidget* w) {
            _set.insert(w);
        }

        void rm_sloppy_widget(SloppyTouchWidget* w) {
            _set.remove(w);
        }

    private:
        QSet<SloppyTouchWidget*> _set;
    };

} // namespace Ivory

#endif // _IVORY_TOUCHROUTERLOGIC_HPP_
