/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Scene.hpp"

#include <QComboBox>
#include <QDebug>
#include <QGraphicsItemGroup>
#include <QGraphicsLinearLayout>
#include <QGraphicsProxyWidget>
#include <QGraphicsScene>
#include <QGraphicsSimpleTextItem>
#include <QPainter>
#include <QRectF>
#include <QStringList>
#include <QStringListModel>

#include <cassert>

#include "KeyboardManual.hpp"
#include "OctaveWidget.hpp"
#include "QuitWidget.hpp"
#include "Wheel.hpp"

namespace Ivory
{
    Scene::Scene(QObject* parent) :
        QGraphicsScene(parent),
        _callback(0),
        _background_image( new QPixmap(":/img/brushed_metal.png") )
    {
        setBackgroundBrush(Qt::blue);

        _kb = new KeyboardManual(0);
        _wh = new Wheel(0);
        _lbl_octave = new QGraphicsSimpleTextItem(0);
        _octave = new OctaveWidget(0);
        _ports_proxy = new QGraphicsProxyWidget(0);
        _ports = new QComboBox(0);
        _ports_proxy->setWidget(_ports);
        _ports_list = new QStringListModel(0);
        _ports->setModel(_ports_list);
        _quit = new QuitWidget(0);

        _kb->set_octave_range(-5,5);
        connect( _kb, SIGNAL(octave_changed(int)),
                 _octave, SLOT(set_octave(int)) );
        connect( _quit, SIGNAL(quit()),
                 this, SIGNAL(quit()) );
        connect( _octave, SIGNAL(octave_up()),
                 _kb, SLOT(octave_up()) );
        connect( _octave, SIGNAL(octave_down()),
                 _kb, SLOT(octave_down()) );
        connect( _ports, SIGNAL(currentIndexChanged(const QString&)),
                 this, SIGNAL(request_connect_to_port(const QString&)) );

        _wh->mode(Wheel::PITCH_WHEEL);

        _lbl_octave->setBrush( QColor(Qt::white) );
        _lbl_octave->setFont( QFont("sans", 18) );
        _lbl_octave->setText( tr("OCTAVE") );

        _wh->show();
        _kb->show();
        _lbl_octave->show();
        _octave->show();
        _ports_proxy->show();
        _quit->show();
        addItem(_wh);
        addItem(_kb);
        addItem(_lbl_octave);
        addItem(_octave);
        addItem(_ports_proxy);
        addItem(_quit);

        resize(width(), height()); // sets up layouts

        _router.add_sloppy_widget(_wh);
        _router.add_sloppy_widget(_kb);
        _router.add_sloppy_widget(_octave);
        _router.add_sloppy_widget(_quit);
    }

    Scene::~Scene()
    {
    }

    bool Scene::event(QEvent * ev)
    {
        bool rv;
        rv = _router.event(ev);
        if( ! rv ) {
            rv = Parent_t::event(ev);
        }
        return rv;
    }

    void Scene::drawBackground(QPainter *p, const QRectF& r)
    {
        p->drawPixmap( r, *_background_image, r);
    }

    void Scene::resize(int width, int height)
    {
        // Lay out the top bar (widget-style layout)
        float margin = 5.0;
        float topbar_ht = 60.0;
        float topbar_widget_ht = topbar_ht - 2.0 * margin;
        float x, y;

        QRectF lr = _lbl_octave->boundingRect();
        x = margin;
        y = (topbar_ht - lr.height()) / 2.0; // Possibly y < 0
        _lbl_octave->setPos( x, y );
        x = 2.0 * margin + _lbl_octave->boundingRect().width();
        y = margin;
        _octave->setGeometry( x, y,
                              3.0*topbar_widget_ht,
                              topbar_widget_ht );
        x = width - topbar_ht + margin;
        _quit->setGeometry( x, y,
                            topbar_widget_ht,
                            topbar_widget_ht );
        float cb_wd = _ports_proxy->rect().width();
        float cb_ht = _ports_proxy->rect().height();
        cb_ht = (topbar_widget_ht < cb_ht) ? topbar_widget_ht : cb_ht;
        cb_wd = ( 4.5 > (cb_wd/topbar_widget_ht) ) ? 4.5 * topbar_widget_ht : cb_wd;
        x = _quit->pos().x() - margin - cb_wd;
        y = (topbar_ht - cb_ht) / 2.0;
        QRectF prox_rect = QRectF( x, y, cb_wd, cb_ht );
        QRectF s_rect = sceneRect();
        // Make sure prox_rect is inside the scene.
        if( ! s_rect.contains(prox_rect) ) {
            if(prox_rect.x() < 0) prox_rect.setX(0);
            if(prox_rect.y() < 0) prox_rect.setY(0);
            prox_rect = prox_rect.intersected(s_rect);
        }
        qDebug() << "prox_rect = " << prox_rect;
        _ports_proxy->setGeometry( prox_rect );

        // Set up the balance...
        float wheel_w = 2.0 * topbar_ht;
        _wh->resize(wheel_w, height-topbar_ht);
        _wh->setPos(0, topbar_ht);
        _kb->resize(width-wheel_w, height-topbar_ht);
        _kb->setPos(wheel_w, topbar_ht);
    }

    void Scene::set_callback(ControlEventCallback *cb)
    {
        _callback = cb;
        _kb->set_callback(cb);
        _wh->set_callback(cb);
    }

    void Scene::set_octave(int o)
    {
        _kb->set_octave(o);
    }

    void Scene::octave_up()
    {
        _kb->octave_up();
    }

    void Scene::octave_down()
    {
        _kb->octave_down();
    }

    void Scene::set_avail_ports(const QStringList& list)
    {
        if( -1 == list.indexOf( "" ) ) {
            QStringList cp;
            cp << QString("") << list;
            _ports_list->setStringList(cp);
        } else {
            _ports_list->setStringList(list);
        }
        update();
    }

} // namespace Ivory
