/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "QuitWidget.hpp"

#include <QDebug>
#include <QPainter>

namespace Ivory
{
    QuitWidget::QuitWidget(QGraphicsWidget *parent) :
        SloppyTouchWidget(parent),
        _logic(),
        _image( new QPixmap(":/img/quit.png") )
    {
        QSizePolicy pol(QSizePolicy::Preferred, QSizePolicy::Preferred );
        pol.setHeightForWidth(true);
        setSizePolicy(pol);
        setMinimumSize(10, 10);
    }

    QuitWidget::~QuitWidget()
    {
    }

    void QuitWidget::paint(QPainter *p,
                             const QStyleOptionGraphicsItem *,
                             QWidget *)
    {
        p->drawPixmap( rect(), *_image, _image->rect() );
    }

    bool QuitWidget::touchEvent(Ivory::TouchEvent& ev)
    {
        TouchButtonLogic::event_t output = TouchButtonLogic::NONE;
        TouchButtonLogic::state_t state;
        state = _logic.touchEvent(ev, rect(), &output);
        if(output == TouchButtonLogic::CLICKED)
            emit quit();
        return true;
    }

    QSizeF QuitWidget::sizeHint(Qt::SizeHint which,
                                const QSizeF& constraint) const
    {
        float h, w;
        if(constraint.width() != -1) {
            w = constraint.width();
        } else {
            w = 50;
        }
        if(constraint.height() != -1) {
            h = constraint.height();
        } else {
            h = w;
        }
        QSizeF rv(w,h);
        return rv;
    }

} // namespace Ivory
