/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "KeyboardManual.hpp"

#include <QDebug>
#include <QEvent>
#include <QPainter>
#include <QTouchEvent>

#include <cassert>

#include "TouchEvent.hpp"
#include "KeyboardManual_p.hpp"

namespace Ivory
{

KeyboardManual::KeyboardManual(QGraphicsWidget* parent,
			       int nkeys,
			       int c)
    : SloppyTouchWidget(parent),
      _callback(0)
{
    setAcceptTouchEvents(true);

    _model.reset( new KeyboardModel );
    _model->reset(nkeys, c);
    _model->set_callback(0);
    set_octave_range(-5, 5);
    set_octave(0);

    _view.reset( new KeyboardView );
    _view->reset();
}

KeyboardManual::~KeyboardManual()
{
}

bool KeyboardManual::event(QEvent* e)
{
    bool rv = false;

    switch( e->type() ) {
    case QEvent::TouchBegin:
    case QEvent::TouchUpdate:
    case QEvent::TouchEnd:
	rv = true;
	touchEvent( static_cast<QTouchEvent*>(e) );
	break;
    default:
	rv = QGraphicsWidget::event(e);
    }
    return rv;
}

int KeyboardManual::key_for_coord(const QPointF&  pos)
{
    int key = 0;
    QRectF zone;

    // Coarse search
    for( key = 0 ; ((unsigned)key) < _model->_playing.size() ; ++key ) {
	zone = _view->key_rect(key, *_model);
	if( zone.contains( pos ) )
	    break;
    }
    if( _model->is_black(key) ) return key;

    assert( _model->is_white(key) );
    zone = _view->key_rect(key+1, *_model);
    if( zone.contains( pos ) )
	++key;
    return key;
}

void KeyboardManual::touchEvent(QTouchEvent *ev)
{
    Ivory::TouchEvent iev(*ev);
    ev->accept();
    touchEvent(iev);
}


bool KeyboardManual::touchEvent(Ivory::TouchEvent& ev)
{
    int key;
    std::set<int> playing;
    QPointF pp;
    Ivory::TouchPoint t;
    bool utilized = false;
    bool upd = false;

    foreach(t, ev) {
        switch( t.state() ) {
        case Qt::TouchPointPressed:
        case Qt::TouchPointMoved:
        case Qt::TouchPointStationary:
            pp = t.pos();
            if( rect().contains(pp) ) {
                utilized = true;
                key = key_for_coord(pp);
                playing.insert(key);
            }
            break;
        case Qt::TouchPointReleased:
            upd = true;
#if 0
            if( rect().contains( t.pos() ) ) {
                utilized = true;
                qDebug() << "release";
            }
#endif
            break;
        default:
            assert(false);
        }
    }
    upd = _model->set_playing(playing) || upd;
    if(upd) update();

    return utilized;
}

void KeyboardManual::set_callback(ControlEventCallback *cb)
{
    _callback = cb;
    _model->set_callback(cb);
}

void KeyboardManual::paint(QPainter *p,
                           const QStyleOptionGraphicsItem * /*option*/,
                           QWidget * /*widget*/ )
{
    int k;
    int white_key_count = _model->white_key_count();
    _view->_white_width = float(size().width()) / white_key_count;
    _view->_canvas = size();

    // Paint the background
    p->fillRect( rect(), _view->_white_key );

    // Paint any white keys that are pressed
    QPen pen;
    pen.setColor( _view->_pressed_key );
    for( k=0 ; ((unsigned)k) < _model->_playing.size() ; ++k ) {
	if ( _model->is_playing(k)
	     && _model->is_white(k) ) {
	    QRectF note_space = _view->key_rect(k, *_model);
	    p->fillRect( note_space, _view->_pressed_key );
	}
    }

    // Paint key separations
    pen.setColor( _view->_black_key );
    pen.setWidth( _view->_black_width );
    p->setPen(pen);
    p->drawRect( rect() );
    for( k = 1 ; k < white_key_count ; ++k ) {
	p->drawLine( QLineF( k * _view->_white_width,
				0.0,
				k * _view->_white_width,
				_view->_canvas.height() ) );
    }

    // Paint the black keys
    QColor color;
    QRectF note_space;
    for( k = 0 ; ((unsigned)k) < _model->_playing.size() ; ++k ) {
	if( _model->is_black(k) ) {
	    color = _model->is_playing(k) ? _view->_pressed_key : _view->_black_key;
	    note_space = _view->key_rect(k, *_model);
	    p->fillRect( note_space, color );
	}
    }

}

void KeyboardManual::set_octave_range(int min, int max)
{
    _model->_c_min = MIDDLE_C + (min * 12);
    _model->_c_max = MIDDLE_C + (max * 12);
    _model->all_off();
    if( _model->_c_note < _model->_c_min ) _model->_c_note = _model->_c_min;
    if( _model->_c_note > _model->_c_max ) _model->_c_note = _model->_c_max;

}

void KeyboardManual::set_octave(int o)
{
    int note = MIDDLE_C + (12 * o);
    if((o >= _model->_c_min) && (o <= _model->_c_max)) {
        if( note != _model->_c_note ) {
            _model->all_off();
            _model->_c_note = note;
            emit octave_changed(o);
        }
    }
}

void KeyboardManual::octave_up()
{
    int note = _model->_c_note + 12;
    if(note <= _model->_c_max && note != _model->_c_note) {
        assert( note >= _model->_c_min );
        _model->all_off();
        _model->_c_note = note;
        emit octave_changed( int((note - MIDDLE_C) / 12) );
    }
}

void KeyboardManual::octave_down()
{
    int note = _model->_c_note - 12;
    if((note >= _model->_c_min) && (note != _model->_c_note)) {
        assert( note <= _model->_c_max );
        _model->all_off();
        _model->_c_note = note;
        emit octave_changed( int((note - MIDDLE_C) / 12) );
    }
}

/**************************************************************
  KeyboardModel methods
************************************************************/

/*
 * C       0 *      G      7 *
 * C#/Db   1        G#/Ab  8
 * D       2 *      A      9 *
 * D#/Eb   3        A#/Bb 10
 * E       4 *      B     11 *
 * F       5 *
 * F#/Gb   6
 */
bool KeyboardModel::is_white(int key) const
{
    int delta = key - _c_offset;
    if( delta < 0 ) delta += 12 * (1 + (-delta/12));
    delta %= 12;
    switch(delta) {
    case 0:
    case 2:
    case 4:
    case 5:
    case 7:
    case 9:
    case 11:
	return true;
	break;
    default:
	return false;
    }
    return false;
}

bool KeyboardModel::is_black(int key) const
{
    return ! is_white(key);
}

int KeyboardModel::white_key_count() const
{
    int k, count;
    for( k=0, count=0 ; ((unsigned)k) < _playing.size() ; ++k ) {
	if( is_white(k) )
	    ++count;
    }
    return count;
}

/**************************************************************
  KeyboardView methods
************************************************************/

QRectF KeyboardView::key_rect(int key, const KeyboardModel& m)
{
    float x;
    int k;
    QRectF rv;

    x = 0;

    for( k=0 ; k<key ; ++k ) {
	if( m.is_white(k) ) {
	    x += _white_width;
	}
    }
    if( m.is_white(k) ) {
	rv = QRectF( x,
		     0.0,
		     _white_width,
		     _canvas.height() );
    } else {
	float wid = _white_width * _black_width;
        float xx = x - wid / 2.0;
        if( xx < 0 ) {
            wid += xx;
            xx = 0;
        }
        if( (xx + wid) > _canvas.width() ) {
            wid = _canvas.width() - xx;
        }
        assert( xx < _canvas.width() );
        assert( wid > 0.0 );
        assert( xx >= 0.0 );
        if( key == 0 ) {
            wid *= 1.5;
        }
        if( key == m._playing.size() - 1 ) {
            xx -= wid/2.0;
            wid *= 1.5;
        }
	rv = QRectF( xx,
		     0.0,
		     wid,
		     _canvas.height() * _black_length );
    }
    return rv;		     
}

} // namespace Ivory
