/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "TouchEvent.hpp"

namespace Ivory
{
    TouchPoint::TouchPoint() :
        _id(-1),
        _state(Qt::TouchPointPressed),
        _primary(false),
        _pos(0, 0),
        _last_pos(0, 0),
        _first_pos(0, 0),
        _pressure(1.0),
        _rect(0,0,0,0)
    {
    }

    TouchPoint::TouchPoint(const TouchPoint& other) :
        _id(other._id),
        _state(other._state),
        _primary(other._primary),
        _pos(other._pos),
        _last_pos(other._last_pos),
        _first_pos(other._first_pos),
        _pressure(other._pressure),
        _rect(other._rect)
    {
    }

    TouchPoint::TouchPoint(const QTouchEvent::TouchPoint& other) :
        _id(other.id()),
        _state(other.state()),
        _primary(other.isPrimary()),
        _pos(other.pos()),
        _last_pos(other.lastPos()),
        _first_pos(other.startPos()),
        _pressure(other.pressure()),
        _rect(other.rect())
    {
    }

    TouchPoint::~TouchPoint()
    {
    }

    const TouchPoint& TouchPoint::operator=(const TouchPoint& other)
    {
        _id = other.id();
        _state = other.state();
        _primary = other.isPrimary();
        _pos = other.pos();
        _last_pos = other.lastPos();
        _first_pos = other.startPos();
        _pressure = other.pressure();
        _rect = other.rect();

        return *this;
    }

    void TouchPoint::translate(const QPointF& p)
    {
        _pos += p;
        _last_pos += p;
        _first_pos += p;
        _rect.translate(p);
    }

    TouchEvent::TouchEvent()
    {
    }

    TouchEvent::TouchEvent(const TouchEvent& other) :
        QList<TouchPoint>( static_cast<const QList<TouchPoint> >(other) )
    {
    }

    TouchEvent::TouchEvent(const QTouchEvent& other)
    {
        clear();
        QList<QTouchEvent::TouchPoint> tps = other.touchPoints();
        QTouchEvent::TouchPoint t;

        foreach(t, tps) {
            append( Ivory::TouchPoint(t) );
        }
    }

    TouchEvent::~TouchEvent()
    {
    }

    void TouchEvent::translate(const QPointF& p)
    {
        Iterator it;

        for( it = begin() ; it != end() ; ++it ) {
            it->translate(p);
        }
    }

    const TouchEvent& TouchEvent::operator=(const TouchEvent& other)
    {
        static_cast< QList<TouchPoint> >(*this) = static_cast< const QList<TouchPoint>& >(other);
        return *this;
    }

    const TouchEvent& TouchEvent::operator=(const QTouchEvent& other)
    {
        clear();
        QList<QTouchEvent::TouchPoint> tps = other.touchPoints();
        QTouchEvent::TouchPoint t;

        foreach(t, tps) {
            append( Ivory::TouchPoint(t) );
        }

        return *this;
    }

} // namespace Ivory
