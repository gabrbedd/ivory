/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_TOUCHEVENT_HPP_
#define _IVORY_TOUCHEVENT_HPP_

#include <QList>
#include <QPointF>
#include <QRectF>
#include <QTouchEvent>

class QTouchEvent;

namespace Ivory
{
    class TouchPoint
    {
    public:
        TouchPoint();
        TouchPoint(const TouchPoint& other);
        TouchPoint(const QTouchEvent::TouchPoint& other);
        ~TouchPoint();

        const TouchPoint& operator=(const TouchPoint& other);

        // Implement as subset of QTouchEvent::TouchPoint
        int id () const { return _id; }
        void id(int i) { _id = i; }

        bool isPrimary () const { return _primary; }
        void setPrimary(bool t) { _primary = t; }

        const QPointF& lastPos () const { return _last_pos; }
        void setLastPos(const QPointF& p) { _last_pos = p; }

        const QPointF& pos () const { return _pos; }
        void setPos(const QPointF& p) { _pos = p; }

        qreal pressure () const { return _pressure; }
        void setPressur(qreal p) { _pressure = p; }

        const QRectF&  rect () const { return _rect; }
        void setRect(const QRectF& rect) { _rect = rect; }

        const QPointF& startPos () const { return _first_pos; }
        void setStartPos(const QPointF& p) { _first_pos = p; }

        Qt::TouchPointState state () const { return _state; }
        void setState(Qt::TouchPointState state) { _state = state; }

        void translate(const QPointF& p);

    private:
        int _id;
        Qt::TouchPointState _state;
        bool _primary;
        QPointF _pos;
        QPointF _last_pos;
        QPointF _first_pos;
        qreal _pressure;
        QRectF _rect;
    };

    class TouchEvent : public QList<TouchPoint>
    {
    public:
        typedef QList<TouchPoint> Parent_t;
        typedef Parent_t::Iterator Iterator;
        typedef Parent_t::ConstIterator ConstIterator;

        TouchEvent();
        TouchEvent(const TouchEvent& other);
        TouchEvent(const QTouchEvent& other);
        ~TouchEvent();

        void translate(const QPointF& p);

        const TouchEvent& operator=(const TouchEvent& other);
        const TouchEvent& operator=(const QTouchEvent& other);
    };

} // namespace Ivory

#endif // _IVORY_TOUCHEVENT_HPP_
