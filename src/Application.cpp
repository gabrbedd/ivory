/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Application.hpp"

#include <QDebug>
#include <QGLWidget>
#include <QGraphicsLinearLayout>
#include <QGraphicsScene>
#include <QGraphicsView>
#include <QGraphicsWidget>
#include <QMutexLocker>
#include <QRectF>
#include <QVBoxLayout>

#include <cassert>
#include <stdexcept>

#include "Scene.hpp"
#include "MidiEngine.hpp"

namespace Ivory
{
    Application::Application(int /*argc*/, char* /*argv*/[]) :
        _scene(0),
        _midi(),
        _functor(0)
    {
        _scene = new Scene(this);
        if( 0 == _scene )
            throw std::runtime_error("Could not allocate memory for Scene object");
        _scene->set_callback(&_functor);

        _midi.reset( new MidiEngine );
        _midi->initialize();
        _midi->activate();

        _functor.set_target(_midi.get());

        setAttribute(Qt::WA_AcceptTouchEvents);
        setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
        setFrameShape(QFrame::NoFrame);

        setScene(_scene);
        setViewport(new QGLWidget);

        _scene->set_octave(0);
        QStringList ports;
        _midi->query_port_list(ports);
        _scene->set_avail_ports(ports);

        resize( 800.0, 480.0 ); // typ. N900 size... default.

        connect( _scene, SIGNAL(quit()),
                 this, SIGNAL(quit()) );
        connect( _scene, SIGNAL(request_connect_to_port(const QString&)),
                 this, SLOT(connect_to_port(const QString&)) );

        show();
    }

    Application::~Application()
    {
        _midi->deactivate();
        _midi->deinitialize();
    }

    void Application::resizeEvent(QResizeEvent *ev)
    {
        _scene->resize(ev->size().width(), ev->size().height());
    }

    void Application::Callback::operator()(const ControlEvent& ev) {
        if(_midi) _midi->control_event(ev);
    }

    void Application::connect_to_port(const QString& dest)
    {
        _midi->connect_to_port(dest);
    }

} // namespace Ivory
