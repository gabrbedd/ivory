/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_WHEEL_HPP_
#define _IVORY_WHEEL_HPP_

#include <QGraphicsWidget>
#include <QSize>
#include <QTouchEvent>
#include <memory>

#include "SloppyTouchWidget.hpp"

class QPaintEvent;
class QPixmap;

namespace Ivory
{
    class ControlEventCallback;
    class TouchEvent;

    /**
     * /brief Container for the widgets at top of the controller
     */
    class Wheel : public SloppyTouchWidget
    {
        Q_OBJECT
    public:
        typedef enum { PITCH_WHEEL = 0,
                       MOD_WHEEL = 1 } wheel_mode_t;

        Wheel(QGraphicsWidget *parent);
        ~Wheel();

        Q_PROPERTY(wheel_mode_t wheel_mode READ mode WRITE mode);
        wheel_mode_t mode();
        void mode(wheel_mode_t m);

        virtual QSize sizeHint() const;
        bool touchEvent(Ivory::TouchEvent& ev);

        void set_callback(ControlEventCallback *cb);

    protected:
        virtual bool event(QEvent *ev);
        virtual void touchEvent(QTouchEvent *ev);
        virtual void paint(QPainter *painter,
                           const QStyleOptionGraphicsItem *option,
                           QWidget *widget);

    private:
        wheel_mode_t  _mode;
        float         _pos;  // Range: [0.0, 1.0], pitch wheel @ 0.5
        ControlEventCallback *_callback;

        std::auto_ptr<QPixmap> _i_base;
        std::auto_ptr<QPixmap> _i_nub;
        std::auto_ptr<QPixmap> _i_edges;
    };

} // namespace Ivory

#endif // _IVORY_WHEEL_HPP_
