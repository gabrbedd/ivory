/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_MIDIENGINEPRIVATE_HPP_
#define _IVORY_MIDIENGINEPRIVATE_HPP_

#include <QMutex>
#include <jack/jack.h>
#include "RingBuffer.hpp"

namespace Ivory
{

    class MidiEnginePrivate
    {
    public:
        MidiEnginePrivate() :
            client(0),
            port(0),
            queue(4096)
            {
            }

        jack_client_t *client;
        jack_port_t   *port;

        QMutex queue_write_mutex;
        Tritium::RingBuffer<MidiEngine::byte_t> queue;

    public:
        jack_nframes_t convert_timestamp_to_jack_time(
            jack_time_t timestamp,
            jack_nframes_t nframes );

    };

} // namespace Ivory

#endif // _IVORY_MIDIENGINEPRIVATE_HPP_
