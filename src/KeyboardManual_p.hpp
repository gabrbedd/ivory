/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */
#ifndef _IVORY_KEYBOARDMANUAL_P_HPP_
#define _IVORY_KEYBOARDMANUAL_P_HPP_

#include <set>

#include "ControlEvent.hpp"

#define MIDDLE_C 60

namespace Ivory
{
    class KeyboardModel
    {
    public:
        typedef std::vector<bool> sequence_t;
        typedef std::set<int> set_t;

        sequence_t _playing;
        int _c_offset;
        ControlEventCallback *_callback;
        int _c_min;
        int _c_max;
        int _c_note; // Actual C note... not octave number.

        void note_on(int key) {
            int note = _c_note + key - _c_offset;
            if( (note >= 0) && (note <= 0x7F) ) {
                NoteEvent ev;
                ev.set_note_event_type(NoteEvent::NoteOn);
                ev.note( note );
                ev.velocity(1.0f);
                (*_callback)(ev);
            }
            _playing[key] = true;
        }
        void note_off(int key) {
            int note = _c_note + key - _c_offset;
            if( (note >= 0) && (note <= 0x7F) ) {
                NoteEvent ev;
                ev.set_note_event_type(NoteEvent::NoteOff);
                ev.note( note );
                ev.velocity(0.0f);
                (*_callback)(ev);
            }
            _playing[key] = false;
        }
        bool set_playing(const set_t& keys) {
            int k;
            bool changes = false;
            for( k = 0 ; (unsigned)k < _playing.size() ; ++k ) {
                if( is_playing(k) ) {
                    if( keys.find(k) == keys.end() ) {
                        note_off(k);
                        changes = true;
                    }
                } else {
                    if( keys.find(k) != keys.end() ) {
                        note_on(k);
                        changes = true;
                    }
                }
            }
            return changes;                
        }

        void all_off() {
            int k;
            for( k = 0 ; (unsigned)k < _playing.size() ; ++k ) {
                if( is_playing(k) ) {
                    note_off(k);
                }
            }
        }

        int is_playing(int key) const { return _playing.at(key); }
        bool is_white(int key) const;
        bool is_black(int key) const;
        int white_key_count() const;

        void reset(int nkeys, int c_off) {
            _playing.clear();
            _playing.assign(nkeys, false);
            if( (c_off < 0)
                || ((unsigned)c_off >= _playing.size()) ) {
                _c_offset = 0;
            } else {
                _c_offset = c_off;
            }
        }

        void set_callback(ControlEventCallback *cb) {
            _callback = cb;
        }
    };

    class KeyboardView
    {
    public:
        QColor _white_key;
        QColor _black_key;
        QColor _pressed_key;
        float _black_length; // Height factor, Range: (0, 1.0)
        float _black_width; // Width factor, Range: (0, 1.0)
        float _line_thickness;
        float _white_width;
        QSizeF _canvas;

        void reset() {
            _white_key = QColor(0xFF, 0xFF, 0xFF);
            _black_key = QColor(0x00, 0x00, 0x00);
            _pressed_key = QColor(0x88, 0x88, 0x88);
            _black_length = 0.66;
            _black_width = 0.5;
            _line_thickness = 5.0;
            _white_width = 25.0;
        }
    
        QRectF key_rect(int key, const KeyboardModel& m);
    };

} // namespace Ivory

#endif // _IVORY_KEYBOARDMANUAL_P_HPP_
