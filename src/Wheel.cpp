/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "Wheel.hpp"

#include <cassert>

#include <QDebug>
#include <QPainter>
#include <QPixmap>

#include "ControlEvent.hpp"
#include "TouchEvent.hpp"

namespace Ivory
{
    Wheel::Wheel(QGraphicsWidget *parent) :
        SloppyTouchWidget(parent),
        _mode(Wheel::PITCH_WHEEL),
        _pos(0.5),
        _i_base( new QPixmap(":/img/wheel_base.png") ),
        _i_nub( new QPixmap(":/img/wheel_nub_inside.png") ),
        _i_edges( new QPixmap(":/img/wheel_nub_edges.png") )
    {
        QSizePolicy pol;

        pol.setHorizontalPolicy(QSizePolicy::Fixed);
        pol.setVerticalPolicy(QSizePolicy::Preferred);
        setSizePolicy(pol);

        setAcceptTouchEvents(true);
    }

    Wheel::~Wheel()
    {
    }

    bool Wheel::event(QEvent *ev)
    {
        int rv = false;
        switch(ev->type()) {
        case QEvent::TouchBegin:
        case QEvent::TouchUpdate:
        case QEvent::TouchEnd:
            rv = true;
            touchEvent( static_cast<QTouchEvent*>(ev) );
            break;
        default:
            rv = QGraphicsWidget::event(ev);
        }
        return rv;
    }

    void Wheel::touchEvent(QTouchEvent *ev)
    {
        Ivory::TouchEvent iev(*ev);
        touchEvent(iev);
    }

    bool Wheel::touchEvent(Ivory::TouchEvent& tp)
    {
        float range = size().height() - size().width();
        float pos = _pos;
        bool utilized = false;

        Ivory::TouchPoint t;
        foreach(t, tp) {
            if( rect().contains( t.pos() ) ) {
                utilized = true;
            }
            switch( t.state() ) {
            case Qt::TouchPointPressed:
            case Qt::TouchPointMoved:
                pos = (t.pos().y() - size().width()/2.0) / range;
                break;
            case Qt::TouchPointStationary:
                pos = (t.pos().y() - size().width()/2.0) / range;
                break;
            case Qt::TouchPointReleased:
                if(_mode == PITCH_WHEEL) pos = 0.5f;
                break;
            default:
                assert(false);
            }
        }
        if( tp.size() == 0 ) pos = 0.5f;
        if( pos < 0.0 ) pos = 0.0f;
        if( pos > 1.0 ) pos = 1.0f;
        if( _pos != pos ) {
            PitchEvent ev;
            ev.value(- (2.0*pos - 1.0));
            (*_callback)(ev);
            _pos = pos;
            update();
        }

        return utilized;
    }

    void Wheel::set_callback(ControlEventCallback *cb)
    {
        _callback = cb;
    }

    QSize Wheel::sizeHint() const
    {
        return QSize(75, 480);
    }

    void Wheel::paint(QPainter *p,
                      const QStyleOptionGraphicsItem * /*option*/,
                      QWidget * /*widget*/)
    {
        float blk = size().width() / 2.0;
        float range = size().height() - blk;
        QRectF base_rect = QRectF( blk/2.0,
                                   blk/2.0,
                                   blk,
                                   range );
        QRectF nub_rect = QRectF( blk/2.0,
                                  0,
                                  blk,
                                  blk );
        nub_rect.translate( 0,
                            (blk/2.0) + _pos * (range-blk) );
        QRectF img_rect = _i_edges->rect();
        img_rect.setHeight( img_rect.width() );
        img_rect.translate( 0,
                            (1.0 - _pos) * (_i_nub->rect().height() - img_rect.height() ) );
        p->drawPixmap( base_rect, *_i_base, _i_base->rect() );
        p->drawPixmap( nub_rect, *_i_nub, img_rect );
        p->drawPixmap( nub_rect, *_i_edges, _i_edges->rect() );
    }

    Wheel::wheel_mode_t Wheel::mode()
    {
        return PITCH_WHEEL;
    }

    void Wheel::mode(Wheel::wheel_mode_t /*m*/)
    {
    }

} // namespace Ivory
