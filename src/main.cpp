/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include <QApplication>
#include <memory>
#include "Application.hpp"

int main(int argc, char* argv[])
{
    QApplication app(argc, argv);
    std::auto_ptr<Ivory::Application> ivory( new Ivory::Application(argc, argv) );

    app.setApplicationName("Ivory");
    app.setApplicationVersion("0.001");
    app.setOrganizationName("Beddingfield");
    app.setOrganizationDomain("gabe.is-a-geek.org");

    QObject::connect( ivory.get(), SIGNAL(quit()),
                      &app, SLOT(quit()) );
    ivory->show();

    return app.exec();
}
