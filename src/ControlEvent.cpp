/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "ControlEvent.hpp"

#include <QDebug>
#include <jack/jack.h>

namespace Ivory
{
    ControlEvent::ControlEvent()
    {
        update_timestamp();
    }

    void ControlEvent::update_timestamp()
    {
        _timestamp = jack_get_time();
    }

    NoteEvent::NoteEvent() :
        _type(NoteEvent::NoteOn),
        _note(36),
        _velocity(1.0f)
    {
    }

    NoteEvent::~NoteEvent()
    {
    }

    void NoteEvent::note(int n)
    {
        if(n < 0)
            _note = 0;
        else
            _note = n;
    }

    void NoteEvent::velocity(float v)
    {
        if(v > 1.0)
            _velocity = 1.0;
        else if (v < 0.0)
            _velocity = 0.0;
        else
            _velocity = v;
    }

    CcEvent::CcEvent() :
        _cc(0),
        _val(0)
    {
    }

    CcEvent::~CcEvent()
    {
    }

    void CcEvent::controller(int cc)
    {
        if( cc > 0x3f ) {
            _cc = 0x3f;
        } else if ( cc < 0 ) {
            _cc = 0;
        } else {
            _cc = cc;
        }
    }

    void CcEvent::value(int val)
    {
        if( val > 0x3f ) {
            _val = 0x3f;
        } else if ( val < 0 ) {
            _val = 0;
        } else {
            _val = val;
        }
    }

    PitchEvent::PitchEvent(float v) :
        _val(v)
    {
    }

    PitchEvent::~PitchEvent()
    {
    }

    void PitchEvent::value(float v)
    {
        if( v > 1.0f ) 
            _val = 1.0f;
        else if( v < -1.0f )
            _val = -1.0f;
        else
            _val = v;
    }

} // namespace Ivory
