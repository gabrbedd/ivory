/* -*- mode: c++; c-basic-offset: 4; indent-tabs-mode: nil; -*-
 * Copyright(c) 2011 by Gabriel M. Beddingfield <gabrbedd@gmail.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY, without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 *
 */

#include "TouchRouterLogic.hpp"
#include "SloppyTouchWidget.hpp"

#include <QDebug>

namespace Ivory
{
    TouchRouterLogic::TouchRouterLogic()
    {
    }

    TouchRouterLogic::~TouchRouterLogic()
    {
    }

    bool TouchRouterLogic::event(QEvent * ev)
    {
        bool rv = false;
        switch(ev->type()) {
        case QEvent::TouchBegin:
        case QEvent::TouchUpdate:
        case QEvent::TouchEnd: {
            ev->accept();
            rv = true;
            Ivory::TouchEvent tev(* static_cast<QTouchEvent*>(ev));
            touchEvent(tev);
        }   break;
        default:
            rv = false;
        }
        return rv;
    }

    bool TouchRouterLogic::touchEvent(Ivory::TouchEvent& ev)
    {
        SloppyTouchWidget *w;
        QMap<SloppyTouchWidget*, TouchEvent> tmap;
        QMap<SloppyTouchWidget*, TouchEvent>::Iterator mi;
        TouchEvent::Iterator ti;

        // Route events to proper widgets
        for(ti = ev.begin() ; ti != ev.end() ; ++ti) {
            QPointF pos = ti->pos();
            bool hit = false;
            foreach(w, _set) {
                if(w->sceneBoundingRect().contains(pos)) {
                    hit = true;
                    if( ! tmap.contains(w) ) {
                        tmap.insert(w, TouchEvent());
                    }
                    tmap[w].append(*ti);
                }
            }
            if( ! hit ) {
                qDebug() << "Dropped a touch event at " << pos;
            }
        }
        for(mi = tmap.begin() ; mi != tmap.end() ; ++mi) {
            mi.value().translate( - mi.key()->scenePos() );
        }

        bool rv = false;
        TouchEvent nil;
        foreach(w, _set) {
            if( tmap.contains(w) ) {
                rv = w->touchEvent( tmap[w] ) || rv;
            } else {
                w->touchEvent(nil);
            }
        }

        return rv;
    }

} // namespace Ivory
