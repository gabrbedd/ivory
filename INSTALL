Ivory - Virtual Keyboard Controller

BUILDING AND INSTALLING IVORY
=============================

Contents:

1. System Requirements
2. Download
3. Binary Packages
4. Prerequisites to Build from Source
5. Build and Install from Source (Simple)
6. INSTALL Changelog

1. System Requirements
----------------------

Ivory is supported on the following operating systems:

  * GNU/Linux (esp. MeeGo 1.2)

Ivory is not a very heavy application, but for good performance you will need:

  - A video card with OpenGL acceleration.
  - A multi-touch input device (touchscreen)

2. Download
-----------

Ivory's source code can be downloaded for the home page:

	http://www.teuton.org/~gabriel/ivory/

The source code for the current development version can be checked out
via Git:

	$ git clone git://gitorious.org/ivory/ivory.git

3. Binary Packages
------------------

There are currently no binary packages available.

4. Prerequisites to Build from Source
-------------------------------------

In order to build from source, you will need the following libraries
installed on your system, and the development header files.

	REQUIRED
	* Qt 4 Library (>=4.7.0)
	* Qt 4 SDK (moc, uic, etc.)
	* GNU g++ compiler (>=4.0, 3.x might work)
	* JACK (jack1 >= 0.109.0, jack2 >= 1.9.3, earlier ones may
          work on some systems)

5. Build and Install from Source (Simple)
-----------------------------------------

After you have all the prerequisites, building and installing will
look like this:

	$ tar xjf ivory-0.500.tar.bz2
	$ cd ivory-0.500
	$ qmake
	$ make
	$ sudo make install

To change the directory where Ivory is installed, it is done like
this on the last step:

	$ sudo make install DESTDIR=/mnt/chroot

THERE IS NO UNINSTALL. USE A PACKAGE MANAGER.

6. INSTALL Changelog
--------------------

2011-03-02 Gabriel M. Beddingfield <gabrbedd@gmail.com>
	* Copied INSTALL.txt from StretchPlayer and modified it.
